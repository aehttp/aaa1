/*file name: kuts_lab_1_1.cpp
*�������: ���� ����� ������������
*����� : ��-1-3
*���� ���������: 15.09.2021
*���� �������� ����: 19.09.2021
*����������� �1
*�������� :��������� ����-����� ��������� ��� ���������� ������� , �� ������ �������� , �� ���������� ���� ����� C++ �������� ������� �������������� ��������.
*����������� ������������ �����: ���������� ����� ������ ,Z1,Z2,Z3 �� ������ ��������� 
*������ : �4
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std;
void SetColor(int text ,int background) {
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdOut, (WORD)((background <<4)|text));	
}
int main(int argc,char**argv) {
	setlocale(LC_ALL,"RUS");
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	system("color 55");
	const int A=1,C=2,F=2,D=2,E=2,H=4,K=2,L=1;
	const double B=0.25;
	double Z1,Z2,Z3,a;
	cout<<"������ �������� ����� a = ";
	cin>>a;
	Z1=A-B-pow(F*sin(a),C)+cos(D*a) ;
	Z2=pow(cos(a),E)+pow(cos(a),H);
	Z3=K*Z1+(L/Z2);
	SetColor(10,0);
	printf("��������� Z1= %5.2f\n",Z1);
	SetColor(11,1	);
	printf("��������� Z2= %5.1f\n",Z2);
	SetColor(4,8);
	printf("��������� Z3= %5.5f\n",Z3);
	SetColor(7,0);
	system("pause");
	return 0;
}
